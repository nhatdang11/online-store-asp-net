﻿$(function () {
    $('input').keydown(function (e) {
        if (e.keyCode == 13) {
            $("input[value='OK']").focus().click();
            return false;
        }
    });
});