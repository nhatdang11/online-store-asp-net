﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyStore.Repository;
using MyStore.ViewModels;
using MyStore.Models;

namespace MyStore.Controllers
{
    public class HomeController : Controller
    {
        private MyStoreDbContext db = new MyStoreDbContext();
        private HomeViewModel homeModel = new HomeViewModel();
        private SearchViewModel searchModel = new SearchViewModel();

        public ActionResult Index()
        {
            homeModel.products = db.Products;
            homeModel.lastestProducts = db.Products.OrderByDescending(p => p.Id).Take(5);
            homeModel.subCategories = db.SubCategories;
            homeModel.featuredProduct1 = db.Products.Find(3);
            homeModel.featuredProduct2 = db.Products.Find(4);
            return View(homeModel);
        }

        public ActionResult Search(string searchInput)
        {
            searchModel.Products = db.Products.Where(p => (p.Name.Contains(searchInput) || p.SubCategory.Name.Contains(searchInput) || p.Brand.Name.Contains(searchInput)));
            searchModel.SuggestedProducts = GetSuggestedProducts(5);
            searchModel.searchString = searchInput;
            return View(searchModel);
        }

        public List<Product> GetSuggestedProducts(int amount)
        {
            var rand = new Random();
            var suggestedProducts = new List<Product>();
            var count = db.Products.Count();
            var randomList = new List<int>();
            int index;
            for (int i = 0; i < amount; i++)
            {
                do
                {
                    index = rand.Next(count);
                } while (randomList.Contains(index));
                randomList.Add(index);
                suggestedProducts.Add(db.Products.ToList()[(index)]);
            }

            return suggestedProducts;
        }
    }
}