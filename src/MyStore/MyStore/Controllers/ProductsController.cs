﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MyStore.Models;
using MyStore.Repository;
using MyStore.ViewModels;

namespace MyStore.Controllers
{
    public class ProductsController : Controller
    {
        private MyStoreDbContext db = new MyStoreDbContext();
        private ProductViewModel model = new ProductViewModel();

        // GET: Products
        public ActionResult Index(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            model.currentProduct = db.Products.Find(id);
            if (model.currentProduct == null)
            {
                return HttpNotFound();
            }

            model.ProductWarehouse = db.ProductWarehouse.Where(p => p.Product.Id == id);
            model.ProductSingleImages = db.ProductSingleImage.Where(p => p.Product.Id == id);
            return View(model);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}