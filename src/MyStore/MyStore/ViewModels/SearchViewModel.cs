﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MyStore.Models;

namespace MyStore.ViewModels
{
    public class SearchViewModel
    {
        public IEnumerable<Product> Products { get; set; }

        public IEnumerable<Product> SuggestedProducts { get; set; }

        public string searchString { get; set; }
    }
}