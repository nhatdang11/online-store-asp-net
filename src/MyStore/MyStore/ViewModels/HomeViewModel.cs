﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MyStore.Models;

namespace MyStore.ViewModels
{
    public class HomeViewModel
    {
        public IEnumerable<Product> products { get; set; }

        public IEnumerable<Product> lastestProducts { get; set; }

        public IEnumerable<SubCategory> subCategories { get; set; }

        public Product featuredProduct1 { get; set; }

        public Product featuredProduct2 { get; set; }
    }
}