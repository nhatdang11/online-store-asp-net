﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MyStore.Models;

namespace MyStore.ViewModels
{
    public class ProductViewModel
    {
        public Product currentProduct { get; set; }

        public IEnumerable<ProductWarehouse> ProductWarehouse { get; set; }

        public IEnumerable<ProductSingleImage> ProductSingleImages { get; set; }
    }
}