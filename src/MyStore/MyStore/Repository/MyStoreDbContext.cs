﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using MyStore.Models;

namespace MyStore.Repository
{
    public class MyStoreDbContext : DbContext
    {
        public MyStoreDbContext() : base("MyStoreDbContext")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<MyStoreDbContext, MyStore.Migrations.Configuration>());
        }

        public DbSet<Product> Products { get; set; }

        public DbSet<SubCategory> SubCategories { get; set; }

        public DbSet<Brand> Brands { get; set; }

        public DbSet<Category> Categories { get; set; }

        public DbSet<Gender> Genders { get; set; }

        public DbSet<ProductWarehouse> ProductWarehouse { get; set; }

        public DbSet<ProductSingleImage> ProductSingleImage { get; set; }
    }
}