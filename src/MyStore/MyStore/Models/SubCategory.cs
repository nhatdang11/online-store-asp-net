﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyStore.Models
{
    public class SubCategory
    {
        public int Id { get; set; }

        public Category Category { get; set; }

        public Gender Gender { get; set; }

        public string Name { get; set; }
    }
}