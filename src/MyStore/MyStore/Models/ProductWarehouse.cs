﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyStore.Models
{
    public class ProductWarehouse
    {
        public int Id { get; set; }

        public Product Product { get; set; }
        
        public string Size { get; set; }

        public int Amount { get; set; }

        public int Sold { get; set; }
    }
}