﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyStore.Models
{
    public class Product
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public decimal Price { get; set; }

        public SubCategory SubCategory { get; set; }

        public Brand Brand { get; set; }

        public string Image { get; set; }
    }
}