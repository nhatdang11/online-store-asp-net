﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyStore.Models
{
    public class ProductSingleImage
    {
        public int Id { get; set; }

        public Product Product { get; set; }

        public string imageUrl { get; set; }

        public string thumbUrl { get; set; }
    }
}