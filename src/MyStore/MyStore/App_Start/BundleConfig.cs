﻿using System.Web;
using System.Web.Optimization;

namespace MyStore
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/main").Include(
                        "~/Scripts/Theme/bootstrap.min.js",
                        "~/Scripts/Theme/jquery-3.2.1.min.js",
                        "~/Scripts/Theme/jquery-ui.min.js",
                        "~/Scripts/Theme/jquery.nicescroll.min.js",
                        "~/Scripts/Theme/jquery.slicknav.min.js",
                        "~/Scripts/Theme/jquery.zoom.min.js",
                        "~/Scripts/Theme/owl.carousel.min.js",
                        "~/Scripts/Theme/main.js",
                        "~/Scripts/Theme/map.js",
                        "~/Scripts/Theme/search-enter.js"
                        ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/css/bootstrap.min.css",
                      "~/Content/css/font-awesome.min.css",
                      "~/Content/css/flaticon.css",
                      "~/Content/css/slicknav.min.css",
                      "~/Content/css/jquery-ui.min.css",
                      "~/Content/css/owl.carousel.min.css",
                      "~/Content/css/animate.css",
                      "~/Content/css/style.css"));
        }
    }
}